import sys
import os.path
import cv2
import webcolors

from PIL import Image, ImageDraw


def closest_color(requested_color):
    min_colors = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_color[0]) ** 2
        gd = (g_c - requested_color[1]) ** 2
        bd = (b_c - requested_color[2]) ** 2
        min_colors[(rd + gd + bd)] = name
    return min_colors[min(min_colors.keys())]


def get_color_hex(requested_color):
    try:
        closest_name = webcolors.rgb_to_name(requested_color)
    except ValueError:
        closest_name = closest_color(requested_color)

    closest_hex = webcolors.name_to_hex(closest_name)

    return closest_hex.replace('#', '')


def check_tile(hex):
    hex = hex.lower()
    if not os.path.isfile('tiles/{}.png'.format(hex)):
        w, h = 75, 75
        shape = [(0, 0), (w, h)]

        tile = Image.new("RGB", (w, h))

        im = ImageDraw.Draw(tile)
        im.rectangle(shape, fill='#{}'.format(hex), outline='#{}'.format(hex))
        tile.save('./tiles/{}-tile.png'.format(hex))


def main():
    src_img = sys.argv[1]
    img_h = int(sys.argv[2])
    img_w = int(sys.argv[3])
    output_file = src_img.split('/')[-1].split('.')[0]
    image = Image.open(src_img)
    image = image.resize([img_w, img_h])

    slack_img = ''
    for y in range(0, img_h):
        for x in range(0, img_w):
            pixel = image.getpixel((x, y))
            if len(pixel) > 3:
                if pixel[3] <= 10:
                    slack_img += ':blank:'
                    continue
            
            color = get_color_hex(pixel[:3])
            check_tile(color)
            slack_img += ':{}-tile:'.format(color)
        slack_img += '\n'

    with open('./copypasta/{}.txt'.format(output_file), 'w+') as f:
        f.write(slack_img)


if __name__ == '__main__':
    main()
